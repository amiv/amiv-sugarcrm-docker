FROM php:5.6-apache

ENV MAJOR_VERSION 6.1
ENV MINOR_VERSION 8
ENV SOURCEFORGE_MIRROR http://master.dl.sourceforge.net
ENV WWW_FOLDER /var/www/html

RUN apt-get update && apt-get upgrade -y && \
    apt-get install -y libcurl4-gnutls-dev libpng-dev unzip cron re2c python libc-client-dev libkrb5-dev && rm -r /var/lib/apt/lists/*

RUN docker-php-ext-install mysql curl gd zip mbstring
#	apt-get install -y php5-mysql php5-imap php5-curl php5-gd curl unzip cron

WORKDIR /tmp
RUN echo "${SOURCEFORGE_MIRROR}/project/sugarcrm/OldFiles/SugarCRM%20Release%20Archive/SugarCRM%20${MAJOR_VERSION}.0/SugarCommunityEdition-${MAJOR_VERSION}.0/SugarCE-${MAJOR_VERSION}.${MINOR_VERSION}.zip"
#					 "${SOURCEFORGE_MIRROR}/project/sugarcrm/OldFiles/SugarCRM%20Release%20Archive/SugarCRM%20${MAJOR_VERSION}.X/SugarCommunityEdition-${MAJOR_VERSION}.X/SugarCE-${MAJOR_VERSION}.${MINOR_VERSION}.zip"
RUN curl -O	"${SOURCEFORGE_MIRROR}/project/sugarcrm/OldFiles/SugarCRM%20Release%20Archive/SugarCRM%20${MAJOR_VERSION}.0/SugarCommunityEdition-${MAJOR_VERSION}.0/SugarCE-${MAJOR_VERSION}.${MINOR_VERSION}.zip" && \
	unzip SugarCE-${MAJOR_VERSION}.${MINOR_VERSION}.zip && \
	rm -rf ${WWW_FOLDER}/* && \
	cp -R /tmp/SugarCE-Full-${MAJOR_VERSION}.${MINOR_VERSION}/* ${WWW_FOLDER}/ && \
	chown -R www-data:www-data ${WWW_FOLDER}/* && \
	chown -R www-data:www-data ${WWW_FOLDER}

# RUN sed -i 's/^upload_max_filesize = 2M$/upload_max_filesize = 10M/' /usr/local/etc/php/php.ini

RUN docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
    && docker-php-ext-install imap

ADD config_override.php.pyt /usr/local/src/config_override.php.pyt
ADD envtemplate.py /usr/local/bin/envtemplate.py
ADD init.sh /usr/local/bin/init.sh

RUN chmod u+x /usr/local/bin/init.sh

ADD crons.conf /root/crons.conf
RUN crontab /root/crons.conf

EXPOSE 80
ENTRYPOINT ["/usr/local/bin/init.sh"]


# sudo docker run --name some-mariadb -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mariadb:latest
# sudo docker run --name myadmin -d --link some-mariadb:db -p 8080:80 phpmyadmin/phpmyadmin
#
#